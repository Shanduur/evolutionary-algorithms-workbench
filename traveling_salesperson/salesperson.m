clear all; close all; clc;

% cities 2
x = [3 2 12 7  9  3 16 11 9 2];
y = [1 4 2 4.5 9 1.5 11 8 10 7];
% x = [0 2 6 7 15 12 14 9.5 7.5 0.5];
% y = [1 3 5 2.5 -0.5 3.5 10 7.5 9 10];
Number_of_Cities = size(x, 2); % number of cities

% initial configuration

sets = [ 
     [250, 0.8, 0.2];
%     [100, 0.8, 0.2];
%     [300, 0.8, 0.2];
%     [500, 0.8, 0.2];
%     [300, 0.5, 0.2];
%     [300, 0.7, 0.2];
%     [300, 0.9, 0.2];
%     [300, 0.7, 0.1];
%     [300, 0.7, 0.3];
%     [300, 0.7, 0.5];
];

f_log = fopen('out.log', 'w');
for i = 1:length(sets)
    costs = [];
    results = zeros(10, Number_of_Cities);

    for trial = progress(1:10)
       [results(trial,:), costs(trial)] = run(int32(sets(i,1)), ...
           sets(i,2), sets(i,3), ...
           x, y, Number_of_Cities);
       draw(results(trial,:), Number_of_Cities, x, y, false);
    end

    [~, ix] = min(costs);
    draw(results(ix,:), Number_of_Cities, x, y, true);

    fprintf(f_log, "Population size: %d, n: %f, pm: %f\n" + ...
        "mean costs: %f, min cost: %f\n", ...
        int32(sets(i,1)), sets(i,2), sets(i,3), ...
        mean(costs), min(costs));
    for j = 1:length(results)
        fprintf(f_log, "results:\n%s\n", num2str(results(j,:)));
        fprintf(f_log, "cost: %f\n",costs(j));
    end
end
fclose(f_log);

function [result, final_cost] = run(Population, n, pm, x, y, Number_of_Cities)
    Parents = zeros(Population, Number_of_Cities);
    Offsprings = [];
    Tmax = 1000;

    % calculating distances between cities and saving them in array
    distance_matrix = zeros(Number_of_Cities);
    for i = 1:Number_of_Cities
        for j = 1:Number_of_Cities
            distance_matrix(i,j) = pdist([x(i), y(i); x(j), y(j)]);
        end
    end

    % getting initial random population
    for i = 1:Population
        Parents(i,:) = randperm(Number_of_Cities, Number_of_Cities);
    end

    % main anlgorithm loop
    for i = 1:Tmax
        parents_distances = [];

        % 2.3 Cost Value
        for j = 1:Population
           parents_distances(j) = calculate_cost(Parents(j,:), distance_matrix); 
        end

        % 2.4 Selection operator
        nParents = select_with_prob(Parents, parents_distances, round(n*Population));    

        % 2.5 Crossover operator
        for j = 1:2:round(n*Population)
            index = randsample(1:length(nParents),2);

            % 
            randomParents = nParents(index,:);

            % generate 2 offsprings      
            Offsprings = [Offsprings; crossover(randomParents)];
        end

        % create mutated offsprings
        Offsprings = [Offsprings; mutate(Offsprings, pm)];

        % create new parents
        offs_and_pars = [Parents; Offsprings];
        Parents = select_k_best(offs_and_pars, distance_matrix, Population);

        Offsprings = [];
        nParents = [];
    end

    result = select_k_best(Parents, distance_matrix, 1);
    final_cost = calculate_cost(result, distance_matrix);
end

function draw(result, Number_of_Cities, x, y, final)
    result(Number_of_Cities+1) = result(1);

    x1 = zeros(Number_of_Cities+1, 1);
    y1 = zeros(Number_of_Cities+1, 1);
    for i=1:Number_of_Cities+1
        x1(i) = x(result(i));
        y1(i) = y(result(i));
    end
    hold on;
    scatter(x, y, 'm')
    a = [1:Number_of_Cities]';
    b = num2str(a);
    c = cellstr(b);
    text(x+0.1, y+0.1, c);
    if final
        plot(x1, y1, 'r-');
        saveas(gcf, datestr(now,'./Out/HHMMSSFFF.png'));
        clf(gcf);
    else 
        plot(x1, y1, 'c--');
    end
end

% function returns k best fitting parents
function best = select_k_best(par_and_offs, distance_mat, k)
    best = zeros(k, length(par_and_offs(1,:)));
    
    costs = [];
    
    for i = 1:length(par_and_offs)
        costs(i) = calculate_cost(par_and_offs(i,:), distance_mat);
    end
    
    [~, best_ind] = mink(costs, k);
    for i = 1:k
       best(i,:) = par_and_offs(best_ind(i),:); 
    end
end

% function used to calculate cost/total distance
function cost = calculate_cost(parent, distance_mat)
    cost = 0;
    for i = 1:length(parent)-1
        cost = cost + distance_mat(parent(i), parent(i+1));
    end
    
    cost = cost + distance_mat(parent(1), parent(length(parent)));
end

% function selecting nParents according to probability calculated from
% costs
function selected_parents = select_with_prob(parents, costs, size)
    selected_parents = zeros(size, length(parents(10,:)));
    p = [];
    
    % probability is inversly proportional to distance
    for i = 1:length(costs)
       p(i) = 1/costs(i); 
    end
    
    % cumulative probabilities
    probabilities = [0, cumsum(p/sum(p))];
    
    for i = 1:size
        r = rand;
        ind = find(r>probabilities, 1, 'last');
        selected_parents(i, :) = parents(ind,:);
    end
end

% function for performing crossover on the parents
% crossover operator
function tempOffsprings = crossover(randomParents)
    P1 = randomParents(1,:);
    P2 = randomParents(2,:);
    Number_of_Cities = length(P1);

    O1 = zeros(1, Number_of_Cities);
    O2 = zeros(1, Number_of_Cities);

    O1(1)=P1(1);
    tindex=1;
    x=false;
    while x==false
        tvalue=P2(tindex);
        for i=1:Number_of_Cities
            if P1(i)==tvalue
                tindex=i;
            end
        end 
        if O1(tindex)==0
            O1(tindex)=P1(tindex);
        else
            x=true;
        end
    end

    for j=1:Number_of_Cities
        if O1(j)==0
            O1(j)=P2(j);
        end
    end

    O2(1)=P2(1);
    tindex=1;
    x=false;
    while x==false
        tvalue=P1(tindex);
        for i=1:Number_of_Cities
            if P2(i)==tvalue
                tindex=i;
            end
        end 
        if O2(tindex)==0
            O2(tindex)=P2(tindex);
        else
            x=true;
        end
    end

    for j=1:Number_of_Cities
        if O2(j)==0
            O2(j)=P1(j);
        end
    end
    
    tempOffsprings = [O1; O2];
end

% function that performs mutation on offsprings according to mutation rate
% mutation operator
function mutated_offsprings = mutate(offsprings, mutation_rate)
    mutated_offsprings = offsprings;
    selected_offsprings = uint8(randperm(length(offsprings), ...
        round(mutation_rate*length(offsprings))));
    
    for i=1:length(selected_offsprings)
        tmp = mutated_offsprings(selected_offsprings(i),:);
        mutated_offsprings(selected_offsprings(i),:) = ...
           tmp(randperm(length(tmp)));
    end
end